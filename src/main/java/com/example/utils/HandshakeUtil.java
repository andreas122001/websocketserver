package com.example.utils;

import com.example.excpetions.HandshakeException;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;

public class HandshakeUtil {

    public void performHandshake(InputStream in) throws IOException {
        // TODO:
        //  - Read HTTP request
        //  - Get key
        //  - Generate accept
        //  - Send HTTP response

        HashMap<String, String> headerMap = HTTPUtil.readAndParseUpgradeRequest(in);
        String key = headerMap.get("Sec-WebSocket-Key");
        String keyAccept = getSecWebSocketAccept(key);





    }



    public static String getSecWebSocketAccept(String secWebSocketKey) {
        final String GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
        final String cat = secWebSocketKey + GUID;
        final byte[] sha1 = sha1(cat);
        String base64 = Base64.getEncoder().encodeToString(sha1);
        return base64;
    }

    public static byte[] sha1(String input) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("SHA1");
            return mDigest.digest(input.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new HandshakeException("There was an error getting the SHA1-algorithm: " + e);
        }
    }

}
