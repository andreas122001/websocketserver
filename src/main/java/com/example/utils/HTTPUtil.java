package com.example.utils;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class HTTPUtil {

    public static HashMap<String, String> readAndParseUpgradeRequest(InputStream inputStream) throws IOException {
        DataInputStream in = new DataInputStream(inputStream);

        HashMap<String, String> header = new HashMap<>();
        String headerLine = "";
        char c;
        boolean prevNewLine = false;
        while (((c = (char)in.readByte()) != '\u0000')) {
            headerLine += c;
            if (c == '\n') {
                if (!prevNewLine) {
                    System.out.print(headerLine);
                    String[] headerLineSplit = headerLine.split(": ");
                    if (headerLineSplit.length == 2)
                        header.put(headerLineSplit[0].trim(), headerLineSplit[1].replace("\n","").trim());
                    else header.put("REQUEST", headerLine);
                    headerLine = "";
                } else {
                    break;
                }
                prevNewLine = true;
            } else if (c != '\r') prevNewLine = false;
        }
        return header;
    }
}
