package com.example.utils;

import java.util.Arrays;

public class BitUtil {

    /**
     * Builds a byte from an integer-array of bits.
     * The array must have a length of 8 and can only contain the digits 0 or 1.
     * @param bits to build with.
     * @return a byte using the bits.
     */
    public static byte buildByte(int[] bits) {
        if (bits.length != 8)
            throw new IllegalArgumentException("A byte must be 8 bits long.");
        if (!Arrays.stream(bits).allMatch(b -> b == 1 || b == 0))
            throw new IllegalArgumentException("Bits can only contain 0 and/or 0");
        byte byte_ = 0;
        for (int b : bits) {
            if (b == 0) {
                byte_ = (byte) (byte_<< 1);
            } else if (b == 1) {
                byte_ = (byte) ((byte_<<1) + 1);
            }
        }
        return byte_;
    }

    /**
     * Returns a bit from a byte at {@code position} from left to right.
     * @param data to get from.
     * @param position of bit.
     * @return bit at position in data.
     */
    public static byte getBit(byte data, int position) {
        return getBits(data, position, 1);
    }

    /**
     * Returns {@code num} bits from {@code data} from {@code offset} from left to right.
     * @param data to get from.
     * @param offset from the left.
     * @param num number of bits to get.
     * @return byte containing the bits from {@code data}.
     */
    public static byte getBits(byte data, int offset, int num) {
        // Shift byte
        int shifted = (data>>>8-(num+offset));
        // Mask byte
        int masked = (int) (Math.pow(2,num)-1);
        return (byte) (shifted & masked);
    }

    /**
     * Prints a given byte as a string of ones and zeroes.
     * E.g. printByte((byte) 10) prints the string "00001010".
     * @param b byte to print.
     */
    public static void printByte(int b) {
        for (int i = 0; i < 8; i++) {
            System.out.print(b>>(7-i)&0b1);
        }
        System.out.println();
    }
}
