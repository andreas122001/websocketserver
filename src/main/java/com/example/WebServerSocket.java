package com.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServerSocket {

    private final ServerSocket serverSocket;

    public WebServerSocket(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public Socket accept() throws IOException {
        Socket socket = serverSocket.accept();

        // TODO:
        //  - Read HTTP handshake request
        //  - Generate accept-key using SHA1
        //  - Send HTTP response, upgrading to websocket
        //  - Create a WebSocket-object and return it

        return socket;
    }
}
