package com.example;

import com.example.model.WSResponseFrame;
import com.example.utils.HandshakeUtil;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;

public class WebSocketWorker extends Thread {

    private final Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private WebSocketServer server;

    public WebSocketWorker(Socket socket, WebSocketServer server) {
        this.socket = socket;
        this.server = server;
    }

    @Override
    public void run() {
        System.out.println("Socket connected on port: " + socket.getPort());

        try (socket) {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());

            String headerLine = "";
            HashMap<String, String> header = new HashMap<>();
            char c;
            boolean prevNewLine = false;
            while (((c = (char)in.readByte()) != '\u0000')) {
                headerLine += c;
                if (c == '\n') {
                    if (!prevNewLine) {
                        System.out.print(headerLine);
                        String[] headerLineSplit = headerLine.split(": ");
                        if (headerLineSplit.length == 2)
                            header.put(headerLineSplit[0].trim(), headerLineSplit[1].replace("\n","").trim());
                        else header.put("REQUEST", headerLine);
                        headerLine = "";
                    } else {
                        break;
                    }
                    prevNewLine = true;
                } else if (c != '\r') prevNewLine = false;
            }

            String key = header.get("Sec-WebSocket-Key");
            System.out.println("Generating accept-key from key: " + key);
            String accept = HandshakeUtil.getSecWebSocketAccept(key);

            System.out.println("Sending handshake accept...: " + accept);
            out.write(("HTTP/1.1 101 Switching Protocols\r\n" +
                    "Upgrade: websocket\r\n" +
                    "Connection: Upgrade\r\n" +
                    "Sec-WebSocket-Accept: " + accept + "\r\n" +
                    "Sec-WebSocket-Protocol: chat\r\n" +
                    "\r\n").getBytes());
            out.flush();

            server.addSocket(this);     // spaghetti?
            System.out.println("Reading input...");

            while (!socket.isClosed()) {
                byte b1 = in.readByte();
                byte b2 = in.readByte();

                int FIN = (b1>>7)&0x1;
                int RSV1 = (b1>>6)&0x1;
                int RSV2 = (b1>>5)&0x1;
                int RSV3 = (b1>>5)&0x1;
                int opcode = b1&15;

                int masked = (b2>>7)&0x1;
                int length = b2&127;

                byte[] mask = new byte[4];
                for (int i = 0; i < 4; i++) {
                    mask[i] = in.readByte();
                }


                String input = "";
                byte b;
                for (int i = 0; i < length; i++) {
                    b = in.readByte();
                    input += (char) (b^mask[i%4]);
                }

                System.out.println(socket.getPort() + ": " + input);
                server.broadcast(socket.getPort() + ": " + input);    // spaghetti
            }

        } catch (NullPointerException | IOException ignored) {}
        System.err.println("Socket closed on port: " + socket.getPort());
        server.removeSocket(this); // psst... might throw exception if
                                     // someone sends a message here before closing
    }

    // spaghetti
    public void send(String message) throws IOException {
        out.write(new WSResponseFrame(message).generateFrame());
    }
}