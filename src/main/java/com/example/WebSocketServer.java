package com.example;

import com.example.utils.HTTPUtil;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class WebSocketServer {

    private final int port;
    private ArrayList<WebSocketWorker> workers = new ArrayList<>();

    public WebSocketServer(int port) {
        this.port = port;
    }

    public void start() {
        try (ServerSocket ss = new ServerSocket(port)) {
            System.out.println("Listening on port: " + port);
            while (true) {
                Socket socket = ss.accept();

                new WebSocketWorker(socket, this).start();
            }
        } catch (IOException e){
            System.err.println("Error on server: " + e);
        }
    }

    // spaghetti
    public void broadcast(String input) {
        for (WebSocketWorker worker : workers) {
            try {
                worker.send(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // spaghetti
    public void addSocket(WebSocketWorker worker) {
        workers.add(worker);
    }

    // spaghetti
    public void removeSocket(WebSocketWorker worker) {
        workers.remove(worker);
    }


}
