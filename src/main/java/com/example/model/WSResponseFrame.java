package com.example.model;

import static com.example.utils.BitUtil.*;

import java.nio.charset.StandardCharsets;

public class WSResponseFrame {

    private static final int FIN = 0b1;
    private static final int RSV1 = 0b0;
    private static final int RSV2 = 0b0;
    private static final int RSV3 = 0b0;
    private final int opcode = 0b0001;
    private final int masked = 0b0;
    private final int length; // length of data in number of bytes
    private final String data; // length (bytes)

    // TODO:
    //  Change FIN depending on data length (length > 125 => FIN should be 0)

    /**
     * Constructs a WebSocket-response-frame, header and data.
     * @param data of the frame.
     */
    public WSResponseFrame(String data) {
        if (data.getBytes(StandardCharsets.UTF_8).length > 125)
            throw new IllegalArgumentException("Too long msg, bro");
        this.data = data;
        this.length = data.length();
    }

    /**
     * Generates a frame from class data.
     * @return websocket frame (as byte-array)
     */
    public byte[] generateFrame() {
        byte[] header = generateHeader();
        byte[] dataBytes = data.getBytes();

        byte[] frame = new byte[header.length + dataBytes.length];

        // Stitches header and data into one frame
        int i = 0;
        for (byte b : header) {
            frame[i] = b;
            i++;
        }
        for (byte b : dataBytes) {
            frame[i] = b;
            i++;
        }
        return frame;
    }

    /**
     * Generate a two-byte frame header from class data.
     * @return frame header.
     */
    public byte[] generateHeader() {
        byte byte1 = buildByte(new int[]{
                FIN, RSV1, RSV2, RSV3,
                getBit((byte) opcode,4),
                getBit((byte) opcode,5),
                getBit((byte) opcode,6),
                getBit((byte) opcode,7)
        });
        byte byte2 = buildByte(new int[] {
                masked,
                getBit((byte) length,1),
                getBit((byte) length,2),
                getBit((byte) length,3),
                getBit((byte) length,4),
                getBit((byte) length,5),
                getBit((byte) length,6),
                getBit((byte) length,7)
        });
        return new byte[]{byte1, byte2};
    }
}
