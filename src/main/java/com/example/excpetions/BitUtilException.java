package com.example.excpetions;

public class BitUtilException extends RuntimeException {
    public BitUtilException(String message) {
        super(message);
    }
}
