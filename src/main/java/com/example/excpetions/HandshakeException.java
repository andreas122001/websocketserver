package com.example.excpetions;

public class HandshakeException extends RuntimeException {
    public HandshakeException(String message) {
        super(message);
    }
}
