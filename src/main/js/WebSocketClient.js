const net = require('net');

// Simple HTTP server responds with a simple WebSocket client test
const httpServer = net.createServer((connection) => {
  connection.on('data', () => {
    let content = `
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
  </head>
  <body>
    WebSocket test page<br>
  <textarea id="text" readonly="readonly" style="width: 250px; height: 150px" ></textarea><br>
  <input id="input" type="text">
  <button onclick="send()">Send</button>
    <script>
      function send() {
        let input = document.querySelector("input")
        let text = input.value
        ws.send(text)
      }
    
      let ws = new WebSocket('ws://localhost:3001', ['chat']);
      ws.onmessage = event => {
        console.log('Message from server: ' + event.data);
        let textarea = document.querySelector("textarea")
        textarea.value += event.data + '\\n'
      }
      ws.onopen = () => {        
        console.log("Connected!")
        console.log("Sending hello")
        ws.send('Joined!');
      }
      ws.onerror = event => {
        console.log("Error: " + event.data)
      }
      ws.onclose = (event) => {
        console.log("Connection closed: " + event.code+" "+event.reason)
      }
    
    </script>
  </body>
</html>
`;
    connection.write('HTTP/1.1 200 OK\r\nContent-Length: ' + content.length + '\r\n\r\n' + content);
  });
});
httpServer.listen(3000, () => {
  console.log('HTTP server listening on port 3000');
});

// WebSocket server
// const GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
// const wsServer = net.createServer((connection) => {
//   console.log('Client connected');
//
//   connection.on('data', (data) => {
//     console.log('Data received from client: ', data.toString());
//   });
//
//   connection.on('end', () => {
//     console.log('Client disconnected');
//   });
// });
// wsServer.on('error', (error) => {
//   console.error('Error: ', error);
// });
// wsServer.listen(3001, () => {
//   console.log('WebSocket server listening on port 3001');
// });   